import 'package:flutter/material.dart';
import 'package:jsonproject/ui/product_page/product_page.dart';
import 'package:jsonproject/injection.dart';

void main() async {
  await iniGetIt();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

