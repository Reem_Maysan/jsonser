import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jsonproject/core/constent.dart';
import 'package:jsonproject/data/db_helper/entites/typee.dart';
import 'package:jsonproject/data/repository/irepository.dart';
import 'package:jsonproject/models/type_model/type_model.dart';
import 'package:jsonproject/ui/product_page/bloc/products_bloc.dart';
import 'package:jsonproject/ui/product_page/bloc/products_event.dart';
import 'package:jsonproject/ui/product_page/bloc/products_state.dart';
import 'package:jsonproject/data/db_helper/dao/typee_dao.dart';
import 'package:jsonproject/data/repository/repository.dart';

import '../../injection.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  IRepository _iRepository;
  Repository rep;
  var connection;

  final _bloc = sl<ProductsBloc>();



  @override
  void initState() {
    _bloc.add(GetMenu());
    _bloc.add(GetTypee());
    /*
    _bloc.add(AddTypee((b) => b.typee = Typee(
        id: _bloc.state.menus.brakeType.first.id,
        title: _bloc.state.menus.brakeType.first.title))); 
        */
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: _bloc,
        builder: (BuildContext context, ProductsState state) {
          connection = Connectivity().checkConnectivity();
          error(state.error);
          if (state.isLoading) {
            return Center(child: CircularProgressIndicator());
          }
          return Scaffold(
              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  print("ddddddddddddddd");
                  print(state.typees.length);
                  for (var item in state.typees) {
                    print("item id + title: ");
                    print(item.id);
                    print(item.title);
                  }
                },
              ),
              backgroundColor: Colors.black12,
              
              body: Center(
                  child: (connection == ConnectivityResult.wifi)

                      ? ListView.builder(
                          itemBuilder: (BuildContext context, int index) {
                            print("--------yes net ----");
                            if (index < state.menus.brakeType.length) {
                              return ListTile(
                                  leading: Icon(
                                    Icons.arrow_circle_up,
                                    color: Colors.white,
                                  ),
                                  title: Text(
                                    state.menus.brakeType[index].title,
                                    style: TextStyle(color: Colors.white),
                                  ));
                            }
                            return ListTile(
                              title: Text('done'),
                            );
                          },
                        )
                      : ListView.builder(
                          itemBuilder: (BuildContext context, int index) {
                          print("--------nooo net ----");
                          if (index < state.typees.length) {
                            return ListTile(
                                leading: Icon(
                                  Icons.arrow_circle_up,
                                  color: Colors.white,
                                ),
                                title: Text(
                                  state.typees[index].title,
                                  style: TextStyle(color: Colors.white),
                                ));
                          }
                          return ListTile(
                            title: Text('done'),
                          );
                        })));
        });
  }

  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg: errorCode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: AppColor.orangeColor,
          textColor: Colors.white,
          fontSize: 16.0);
      _bloc.add(ClearError());
    }
  }
}
