library products_state;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:jsonproject/data/db_helper/entites/typee.dart';
import 'package:jsonproject/models/type_model/type_list.dart';

part 'products_state.g.dart';

abstract class ProductsState
    implements Built<ProductsState, ProductsStateBuilder> {
  // fields go here

  String get error;

  bool get isLoading;

  bool get isExpanded;

 // @nullable
  BuiltList<Typee> get typees;

  @nullable
  TypeList get menus;

  ProductsState._();

  factory ProductsState([updates(ProductsStateBuilder b)]) = _$ProductsState;

  factory ProductsState.initail() {
    return ProductsState((b) => b
      ..error = ""
      ..isLoading = false
      ..isExpanded = false
      ..typees.replace([])
      ..menus = null);
  }
}
