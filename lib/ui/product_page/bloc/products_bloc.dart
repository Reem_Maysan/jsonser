import 'package:bloc/bloc.dart';
import 'package:jsonproject/data/repository/Irepository.dart';
import 'products_event.dart';
import 'products_state.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  IRepository _repository;

  ProductsBloc(this._repository);

  @override
  ProductsState get initialState => ProductsState.initail();

  @override
  Stream<ProductsState> mapEventToState(
    ProductsEvent event,
  ) async* {
    if (event is GetMenu) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
          ..menus = null);
        print("event is GetMenu: ");

        final data = await _repository.getMenus();
        print('GetMenus Success data: $data');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = ""
          ..menus.replace(data));
      } catch (e) {
        print('GetMenus Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong"
          ..menus = null);
      }
    }
    if (event is AddTypee) {
      try {
        await _repository.insertTypee(event.typee);
        final result = await _repository.getTypee();
        yield state.rebuild((b) => b
          ..isLoading = false
          ..typees.replace(result));
      } catch (e) {
        print('$e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong");
      }
    }

    if (event is GetTypee) {
      yield state.rebuild((b) => b..typees.clear());
      try {
        final result = await _repository.getTypee();
        yield state.rebuild((b) => b
          ..isLoading = false
          ..typees.replace(result));
      } catch (e) {
        print('$e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong");
      }
    }

    if (event is DeleteTypee) {
      try {
        final result = await _repository.deleteTypee(event.id);
        yield state.rebuild((b) => b..isLoading = false);
        try {
          final result = await _repository.getTypee();
          yield state.rebuild((b) => b
            ..isLoading = false
            ..typees.replace(result));
        } catch (e) {
          print('$e');
          yield state.rebuild((b) => b
            ..isLoading = false
            ..error = "Something went wrong");
        }
      } catch (e) {
        print('$e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong");
      }
    }
  }
}
