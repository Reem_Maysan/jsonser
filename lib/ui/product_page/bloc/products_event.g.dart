// GENERATED CODE - DO NOT MODIFY BY HAND

part of products_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetProducts extends GetProducts {
  factory _$GetProducts([void Function(GetProductsBuilder) updates]) =>
      (new GetProductsBuilder()..update(updates)).build();

  _$GetProducts._() : super._();

  @override
  GetProducts rebuild(void Function(GetProductsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetProductsBuilder toBuilder() => new GetProductsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetProducts;
  }

  @override
  int get hashCode {
    return 583255550;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetProducts').toString();
  }
}

class GetProductsBuilder implements Builder<GetProducts, GetProductsBuilder> {
  _$GetProducts _$v;

  GetProductsBuilder();

  @override
  void replace(GetProducts other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetProducts;
  }

  @override
  void update(void Function(GetProductsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetProducts build() {
    final _$result = _$v ?? new _$GetProducts._();
    replace(_$result);
    return _$result;
  }
}

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder) updates]) =>
      (new ClearErrorBuilder()..update(updates)).build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ClearError build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$GetMenu extends GetMenu {
  factory _$GetMenu([void Function(GetMenuBuilder) updates]) =>
      (new GetMenuBuilder()..update(updates)).build();

  _$GetMenu._() : super._();

  @override
  GetMenu rebuild(void Function(GetMenuBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetMenuBuilder toBuilder() => new GetMenuBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetMenu;
  }

  @override
  int get hashCode {
    return 821705771;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetMenu').toString();
  }
}

class GetMenuBuilder implements Builder<GetMenu, GetMenuBuilder> {
  _$GetMenu _$v;

  GetMenuBuilder();

  @override
  void replace(GetMenu other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetMenu;
  }

  @override
  void update(void Function(GetMenuBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetMenu build() {
    final _$result = _$v ?? new _$GetMenu._();
    replace(_$result);
    return _$result;
  }
}

class _$GetTypee extends GetTypee {
  factory _$GetTypee([void Function(GetTypeeBuilder) updates]) =>
      (new GetTypeeBuilder()..update(updates)).build();

  _$GetTypee._() : super._();

  @override
  GetTypee rebuild(void Function(GetTypeeBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetTypeeBuilder toBuilder() => new GetTypeeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetTypee;
  }

  @override
  int get hashCode {
    return 438412095;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('GetTypee').toString();
  }
}

class GetTypeeBuilder implements Builder<GetTypee, GetTypeeBuilder> {
  _$GetTypee _$v;

  GetTypeeBuilder();

  @override
  void replace(GetTypee other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetTypee;
  }

  @override
  void update(void Function(GetTypeeBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetTypee build() {
    final _$result = _$v ?? new _$GetTypee._();
    replace(_$result);
    return _$result;
  }
}

class _$AddTypee extends AddTypee {
  @override
  final Typee typee;

  factory _$AddTypee([void Function(AddTypeeBuilder) updates]) =>
      (new AddTypeeBuilder()..update(updates)).build();

  _$AddTypee._({this.typee}) : super._() {
    if (typee == null) {
      throw new BuiltValueNullFieldError('AddTypee', 'typee');
    }
  }

  @override
  AddTypee rebuild(void Function(AddTypeeBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddTypeeBuilder toBuilder() => new AddTypeeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AddTypee && typee == other.typee;
  }

  @override
  int get hashCode {
    return $jf($jc(0, typee.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AddTypee')..add('typee', typee))
        .toString();
  }
}

class AddTypeeBuilder implements Builder<AddTypee, AddTypeeBuilder> {
  _$AddTypee _$v;

  Typee _typee;
  Typee get typee => _$this._typee;
  set typee(Typee typee) => _$this._typee = typee;

  AddTypeeBuilder();

  AddTypeeBuilder get _$this {
    if (_$v != null) {
      _typee = _$v.typee;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddTypee other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AddTypee;
  }

  @override
  void update(void Function(AddTypeeBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AddTypee build() {
    final _$result = _$v ?? new _$AddTypee._(typee: typee);
    replace(_$result);
    return _$result;
  }
}

class _$DeleteTypee extends DeleteTypee {
  @override
  final int id;

  factory _$DeleteTypee([void Function(DeleteTypeeBuilder) updates]) =>
      (new DeleteTypeeBuilder()..update(updates)).build();

  _$DeleteTypee._({this.id}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('DeleteTypee', 'id');
    }
  }

  @override
  DeleteTypee rebuild(void Function(DeleteTypeeBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DeleteTypeeBuilder toBuilder() => new DeleteTypeeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DeleteTypee && id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(0, id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DeleteTypee')..add('id', id))
        .toString();
  }
}

class DeleteTypeeBuilder implements Builder<DeleteTypee, DeleteTypeeBuilder> {
  _$DeleteTypee _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  DeleteTypeeBuilder();

  DeleteTypeeBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DeleteTypee other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DeleteTypee;
  }

  @override
  void update(void Function(DeleteTypeeBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DeleteTypee build() {
    final _$result = _$v ?? new _$DeleteTypee._(id: id);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
