library products_event;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:jsonproject/data/db_helper/entites/typee.dart';

part 'products_event.g.dart';

abstract class ProductsEvent {}

abstract class GetProducts extends ProductsEvent
    implements Built<GetProducts, GetProductsBuilder> {
  // fields go here

  GetProducts._();

  factory GetProducts([updates(GetProductsBuilder b)]) = _$GetProducts;
}

abstract class ClearError extends ProductsEvent
    implements Built<ClearError, ClearErrorBuilder> {
  // fields go here

  ClearError._();

  factory ClearError([updates(ClearErrorBuilder b)]) = _$ClearError;
}

abstract class GetMenu extends ProductsEvent
    implements Built<GetMenu, GetMenuBuilder> {
  // fields go here

  GetMenu._();

  factory GetMenu([updates(GetMenuBuilder b)]) = _$GetMenu;
}

///Typee

abstract class GetTypee extends ProductsEvent
    implements Built<GetTypee, GetTypeeBuilder> {
  // fields go here

  GetTypee._();

  factory GetTypee([updates(GetTypeeBuilder b)]) = _$GetTypee;
}

abstract class AddTypee extends ProductsEvent
    implements Built<AddTypee, AddTypeeBuilder> {
  // fields go here
  
  //@nullable
  Typee get typee;

  AddTypee._();

  factory AddTypee([updates(AddTypeeBuilder b)]) = _$AddTypee;
}

abstract class DeleteTypee extends ProductsEvent
    implements Built<DeleteTypee, DeleteTypeeBuilder> {
  // fields go here

  int get id;

  DeleteTypee._();

  factory DeleteTypee([updates(DeleteTypeeBuilder b)]) = _$DeleteTypee;
}
