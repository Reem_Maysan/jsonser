import 'package:built_value/serializer.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:jsonproject/models/type_model/type_list.dart';
import 'package:jsonproject/models/type_model/type_model.dart';

part 'serializers.g.dart';

//add all of the built value types that require serialization
@SerializersFor(const [TypeList, TypeModel])

// normal serialization
//final Serializers serializers = _$serializers

// built_value has it's own json serialization, we can create valid typical json like this:

final Serializers serializers = (_$serializers.toBuilder()
      ..addPlugin(StandardJsonPlugin())
      ..addBuilderFactory(
          (FullType(
            BuiltList,
            [
              const FullType(TypeList),
            ],
          )),
          () => ListBuilder<TypeList>()))
    .build();
