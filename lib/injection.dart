import 'package:get_it/get_it.dart';
import 'package:dio/dio.dart';
import 'package:jsonproject/data/repository/Irepository.dart';
import 'package:jsonproject/data/repository/repository.dart';
import 'core/constent.dart';
import 'package:jsonproject/data/http_helper/http_helper.dart';
import 'package:jsonproject/data/http_helper/ihttp_helper.dart';
import 'package:jsonproject/ui/product_page/bloc/products_bloc.dart';
import 'package:jsonproject/data/db_helper/Idb_helper.dart';
import 'package:jsonproject/data/db_helper/db_helper.dart';
import 'package:jsonproject/data/prefs_helper/iprefs_helper.dart';
import 'package:jsonproject/data/prefs_helper/prefs_helper.dart';

final sl = GetIt.instance;

Future iniGetIt() async {
  sl.registerLazySingleton(() => ((Dio(BaseOptions(
      baseUrl: BaseUrl,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "charset": "utf-8",
        "Accept-Charset": "utf-8"
      },
      responseType: ResponseType.plain)))));

  // data

  sl.registerLazySingleton<IDbHelper>(() => DbHelper());
  sl.registerLazySingleton<IPrefsHelper>(() => PrefsHelper());
  sl.registerLazySingleton<IHttpHelper>(() => HttpHelper(sl()));
  sl.registerLazySingleton<IRepository>(() => Repository(sl(), sl(), sl()));

  /// ProductBloc

  sl.registerFactory(() => ProductsBloc(sl()));


}