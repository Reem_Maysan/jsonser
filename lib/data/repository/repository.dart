import 'package:connectivity/connectivity.dart';
import 'package:jsonproject/data/db_helper/Idb_helper.dart';
import 'package:jsonproject/data/db_helper/entites/typee.dart';
import 'package:jsonproject/data/http_helper/ihttp_helper.dart';
import 'package:jsonproject/data/prefs_helper/iprefs_helper.dart';
import 'package:jsonproject/models/type_model/type_list.dart';
import 'package:jsonproject/models/type_model/type_model.dart';

import 'Irepository.dart';

class Repository implements IRepository {
  IHttpHelper _ihttpHelper;
  IPrefsHelper _iprefHelper;
  IDbHelper _iDbHelper;

  Repository(this._ihttpHelper, this._iprefHelper, this._iDbHelper);

  @override
  Future<TypeList> getMenus() async {
    return await _ihttpHelper.getMenus();
  }

  @override
  Future<dynamic> fetchData() async {
    
  }

  @override
  Future<void> insertTypee(Typee type) async {
    try {
      final List<Typee> result = await _iDbHelper.getTypee();
      if (result == null || result.isEmpty) {
        _iDbHelper.insertTypee(type);
      } else {
        for (var item in result) {
          if (item.id == type.id) {
            print('Exception Exception');
            throw Exception("");
          }
        }
        _iDbHelper.insertTypee(type);
      }
    } catch (e) {
      throw Exception("");
    }
  }

  @override
  Future<dynamic> getTypee() async {
   return _iDbHelper.getTypee();
  }

  @override
  Future<void> deleteTypee(int id) {
    return _iDbHelper.deleteTypee(id);
  }
}
