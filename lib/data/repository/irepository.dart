import 'package:jsonproject/data/db_helper/entites/typee.dart';
import 'package:jsonproject/models/type_model/type_list.dart';
import 'package:jsonproject/models/type_model/type_model.dart';

abstract class IRepository {

  Future<TypeList> getMenus();

  //Type
  Future<void> insertTypee(Typee type);

  Future<dynamic> getTypee();

  Future<void> deleteTypee(int id);

  Future<dynamic> fetchData();

}
