import 'package:jsonproject/models/type_model/type_list.dart';

abstract class IHttpHelper {

  Future<TypeList> getMenus();
}
