import 'dart:convert';
import 'package:built_value/serializer.dart';
import 'package:dio/dio.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:jsonproject/models/type_model/type_list.dart';

import 'ihttp_helper.dart';
import 'package:jsonproject/selializers/serializers.dart';


class HttpHelper implements IHttpHelper {
  final Dio _dio;

  var url = "http://bikehub.store/api/customer/menus";

  var cookieJar = CookieJar();

  HttpHelper(this._dio) {
    _dio.interceptors.add(
      LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
      ),
    );
    _dio.interceptors.add(CookieManager(cookieJar));
  }

  @override
  Future<TypeList> getMenus() async {
        print("hi from httphelper getMenus ");

    _dio.interceptors.add(CookieManager(cookieJar));
    try {
      final response = await _dio.get(url);
      if (response.statusCode == 200) {
        print('getMenus Response body  ${response.data}');

        final TypeList baseResponse = serializers.deserialize(
            json.decode(response.data),
            specifiedType: FullType(TypeList));

        print("getMenus status : $baseResponse");
        if (baseResponse != null) {
          return baseResponse;
        }
      }
    } catch (e) {
      print(e.toString());
    }
    throw UnimplementedError();
  }
}
